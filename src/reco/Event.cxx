#include "MuonReco/Event.h"

namespace MuonReco {

  Event::Event() {
    hd       = Signal();
    trl      = Signal();
    sigs     = std::vector<Signal>();
    hits     = std::vector<Hit>();
    clusters = std::vector<Cluster>();
    tracks   = std::vector<Track>();
    nSigs    = 0;
  }


  Event::Event(Signal header, Signal trailer, std::vector<Signal> signals){
    hd       = header;
    trl      = trailer;
    sigs     = signals;
    hits     = std::vector<Hit>();
    clusters = std::vector<Cluster>();
    tracks   = std::vector<Track>();
    nSigs    = 0;
  }

  Event::Event(Signal header, Signal trailer, std::vector<Signal> signals, std::vector<Hit> wirehits,
         std::vector<Cluster> clusts, std::vector<Track> trks) {
    hd       = header;
    trl      = trailer;
    sigs     = signals;
    hits     = wirehits;
    clusters = clusts;
    tracks   = trks;
    nSigs    = 0;
    update();
  }
  Event::Event(const Event &e) {
    hd       = e.Header  ();
    trl      = e.Trailer ();
    sigs     = e.Signals ();
    hits     = e.Hits    ();
    clusters = e.Clusters();
    tracks   = e.Tracks  ();
    update();
  }

  Signal          Event::Header  () const {return hd     ;}
  Signal          Event::Trailer () const {return trl    ;}
  vector<Signal>  Event::Signals () const {return sigs   ;}
  vector<Hit>     Event::Hits    () const {return hits   ;}
  vector<Cluster> Event::Clusters() const {return clusters;}
  vector<Track>   Event::Tracks  () const {return tracks ;}
  
  bool Event::Pass() const {
    return pass;
  }

  void Event::AddHit(Hit h) {
    hits.push_back(h);
    nSigs++;
  }

  void Event::SetPassCheck(bool b) {
    pass = b;
  }
  
  void Event::AddCluster(Cluster c) {
    clusters.push_back(c);
  }

  void Event::AddTrack(Track t) {
    tracks.push_back(t);
  }

  void Event::update() {
    nSigs  = hits.size();
  }

  void Event::CheckClusterTime() {
    for (Cluster c : clusters) {
      for (Hit h : c.Hits()) {
        if (h.CorrTime()>400 || h.CorrTime()<0) {
          hasBadHitTime = kTRUE;
          return;
	      }
      }
    }
  }

  bool Event::AnyWireHit(int TDC, int Channel) {
    for (Hit h : hits) {
      if (h.TDC() == TDC && h.Channel() == Channel)
        return kTRUE;
    }
    return kFALSE;
  }

  void Event::Draw() {}
}
