#include "MuonReco/Signal.h"

namespace MuonReco {

	Signal::Signal() {
		Signal(0);
	}

	Signal::Signal(uint64_t word) {




		bitset<4>  _type;
		bitset<12> _eventid;
		bitset<12> _eventid_t;
		bitset<17> _triggerledge;
		bitset<3>  _csmid;
		bitset<5>  _tdcid;
		bitset<5>  _chnlid;
		bitset<2>  _mode;
		bitset<17> _ledge;
		bitset<8>  _width;
		bitset<10> _hitcount;
		bitset<12> _tdc_eventid;


		_type         = word >> 36;
		_eventid      = word >> 17;
		_eventid_t    = word >> 16;
		_triggerledge = word >>  0;
		_csmid        = word >> 37;
		_tdcid        = word >> 32;
		_chnlid       = word >> 27;
		_mode         = word >> 25;
		_ledge        = word >>  8;
		_width        = word >>  0;
		_hitcount     = word >>  0;
		_tdc_eventid  = word >> 12;

		type          = static_cast<int>((_type.to_ulong()));
		eventid       = static_cast<int>((_eventid.to_ulong()));
		eventid_t     = static_cast<int>((_eventid_t.to_ulong()));
		triggerledge  = static_cast<int>((_triggerledge.to_ulong()));
		csmid         = static_cast<int>((_csmid.to_ulong()));
		tdcid         = static_cast<int>((_tdcid.to_ulong()));
		chnlid        = static_cast<int>((_chnlid.to_ulong()));
		mode          = static_cast<int>((_mode.to_ulong()));
		ledge         = static_cast<int>((_ledge.to_ulong()));
		width         = static_cast<int>((_width.to_ulong()));
		hitcount      = static_cast<int>((_hitcount.to_ulong()));
		tdc_eventid   = static_cast<int>((_tdc_eventid.to_ulong()));
	}

	int Signal:: Type()        {return type        ;}
	int Signal:: HeaderEID()   {return eventid     ;}
	int Signal:: TrailerEID()  {return eventid_t   ;}
	int Signal:: TriggerLEdge(){return triggerledge;}
	int Signal:: CSMID()       {return csmid       ;}
	int Signal:: TDC()         {return tdcid       ;}
	int Signal:: Channel()     {return chnlid      ;}
	int Signal:: Mode()        {return mode        ;}
	int Signal:: LEdge()       {return ledge       ;}
	int Signal:: Width()       {return width       ;}
	int Signal:: HitCount()    {return hitcount    ;}
	int Signal:: TDCHeaderEID(){return tdc_eventid ;}
}
