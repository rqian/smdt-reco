#ifndef MUON_EVENT
#define MUON_EVENT

#include <vector>

#include "MuonReco/RecoObject.h"
#include "MuonReco/Signal.h"
#include "MuonReco/EventID.h"
#include "MuonReco/Hit.h"
#include "MuonReco/Cluster.h"
#include "MuonReco/Track.h"


namespace MuonReco {
  
  /*! \class Event Event.h "MuonReco/Event.h"
   *
   * \brief Event is a container for all reconstructed objects in the reco chain for sMDT chambers
   *
   * A CINT dictionary is generated for Event, as for all objects inheriting from RecoObject, which allows the user to write Event objects to a TTree for easy storage
   *
   * \author Kevin Nelson
   *         kevin.nelson@cern.ch
   * \date   May 21 2019
   */
  class Event : public RecoObject {
  public:
    Event();
    Event(Signal header, Signal trailer, vector<Signal> signals);
    Event(Signal header, Signal trailer, vector<Signal> signals, vector<Hit> wirehits, 
                  vector <Cluster> clusts, vector<Track> trks);
    Event(const Event &e);

    Signal          Header  () const;
    Signal          Trailer () const;
    vector<Signal>  Signals () const;
    vector<Hit>     Hits    () const;
    vector<Cluster> Clusters() const;
    vector<Track>   Tracks  () const;
    bool            Pass    () const;


    std::vector<Cluster> mClusters();

    void   AddHit (Hit h);
    void   update       ();
    void   SetPassCheck (bool b);
    void   AddCluster   (Cluster c);
    void   AddTrack     (Track t);
    bool AnyWireHit   (int TDC, int Channel);

    // temporary
    void CheckClusterTime();

    void Draw() override;


  private:
    Signal          hd;           
    Signal          trl;          
    vector<Signal>  sigs;         
    vector<Hit>     hits;         
    vector<Cluster> clusters;     
    vector<Track>   tracks;       
    int             nSigs         = 0;
    bool            pass          = kFALSE;
    bool            hasBadHitTime = kFALSE;
  };

}

#endif
