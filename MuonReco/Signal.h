#ifndef MUON_SIGNAL
#define MUON_SIGNAL

#include <bitset>
#include <iostream>

#include "MuonReco/EventID.h"

namespace MuonReco {

  /*! \class Signal Signal.h "MuonReco/Signal.h"
   * \brief Wrapper to TDC word on non-trigger channel
   * 
   * Signal is a nice wrapper to a HPTDC word
   * corresponding to a signal leading or falling 
   * edge time
   *
   * The packet format is: 
   *     32 bits total 
   *      4 bit type         (identified rising/falling edge)   
   *      4 bit tdc num      (which board did this come from)
   *      5 bit channel num  (which channel on the board) 
   *     12 bit coarse time  
   *      7 bit fine time   
   *
   * the true event time in nanoseconds is 
   *
   * 25ns*(coarse + fine/128)    
   *
   * For further details, see HPTDC Documentation 
   * https://cds.cern.ch/record/1067476/files/cer-002723234.pdf
   * pages 21-22
   *
   * \author Kevin Nelson
   *         kevin.nelson@cern.ch
   * \date   17 July 2020
   */
  class Signal {
  public:
    Signal(             );
    Signal(uint64_t word);

    int Type();
    int HeaderEID();
    int TrailerEID();
    int TriggerLEdge();
    int CSMID();
    int TDC();
    int Channel();
    int Mode();    
    int LEdge();  
    int Width();
    int HitCount(); 
    int TDCHeaderEID();   
    static const short HEADER  = 0b1010;
    static const short TRAILER = 0b1100;

  private:
    int type        ;
    int eventid     ;
    int eventid_t   ;
    int triggerledge;
    int csmid       ;
    int tdcid       ;
    int chnlid      ;
    int mode        ;
    int ledge       ;
    int width       ;
    int hitcount    ;
    int tdc_eventid ;


  };
}
#endif
